title: URL Shortener (ALL LEVELS)
description: Convert long URLs to short ones
---

# URL Shortener

## Tips for success

* **Relax!** It’s okay if you get stuck. We're interested in how you think through problems with another person. Think out loud! Don't be afraid to ask questions - Your pairing partner is there to help.
* **Ask questions**. We want to understand how you think about and approach problems.
* **Write tests**. Quality is important to us, don’t forget to test your code as you
  go along.
* **Use the Internet!** We want to understand how you find and evaluate online information.
* **Give feedback**. We don’t just want you to succeed, we want you to have fun!

Write a URL shortener.

A URL shortener is a program in which a Uniform Resource Locator (URL) is made substantially shorter while still taking the user to the required page.  

----------

### IMPORTANT

"**Environment Validation**" Step should have been completed to start the challenge, as stated in the email sent prior to the interview, and should be used to develop task 1 below.

If the setup is not complete please use below steps to complete the setup task. 

- Create a function to take an input param of a string and return the upper case of the string. This will the starting point of future improvements.

    ```text
       input: "Hello World!"
       output: "HELLO WORLD!"
    ```

- Create a unit test to verify this functionality
